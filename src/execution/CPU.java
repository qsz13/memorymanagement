package execution;

import GUI.MainFrame;
import memory.Memory;
import program.*;
import program.Process;

/**
 * Created by danielqiu on 5/11/14.
 */
public class CPU {

    Memory memory = new Memory();
    Process process = new Process();

    public void executeFIFO(){
        int times = 0;
        int i = 320;
        int Min = 0;
        int Max = 319;
        int upperBound = 319;
        int lowerBound = 0;
        while(!process.allExecuted())
        {
            times++;

            int index = lowerBound + (int)(Math.random() * ((upperBound - lowerBound) + 1));
            Instruction ins = process.instructions.get(index);
            memory.loadInsructionFIFO(ins);
            process.markExecuted(index);
            if(index <319)
            {
                index++;
                ins = process.instructions.get(index);
                memory.loadInsructionFIFO(ins);
                process.markExecuted(index);
            }
            if (times%2 == 0)
            {
                upperBound = Max;
                if(index < 319)
                {
                    lowerBound = index+1;

                }
            }
            else
            {
                lowerBound = Min;
                if(index > 0)
                {
                    upperBound = index-1;
                }
            }

        }
//"<html><p>命中：</p><p>未命中：</p><p>命中率：</p></html>"

        System.out.println("total" + memory.checkTimes);
        System.out.println("miss" + memory.unhitTimes);

        float hitRate = (float)memory.unhitTimes/(float)memory.checkTimes;
        int hitTimes = memory.checkTimes-memory.unhitTimes;
        MainFrame.FIFOResultLabel.setText("<html><p>总次数："+memory.checkTimes+"</p><p>命中："+hitTimes+
                "</p><p>未命中："+memory.unhitTimes+"</p><p>命中率："+hitRate+"</p></html>");


    }



    public void executeLRU(){
        int times = 0;
        int i = 320;
        int Min = 0;
        int Max = 319;
        int upperBound = 319;
        int lowerBound = 0;
        while(!process.allExecuted())
        {
            times++;

            int index = lowerBound + (int)(Math.random() * ((upperBound - lowerBound) + 1));
            Instruction ins = process.instructions.get(index);
            memory.loadInsructionLRU(ins);
            process.markExecuted(index);
            if(index <319)
            {
                index++;
                ins = process.instructions.get(index);
                memory.loadInsructionLRU(ins);
                process.markExecuted(index);
            }
            if (times%2 == 0)
            {
                upperBound = Max;
                if(index < 319)
                {
                    lowerBound = index+1;

                }
            }
            else
            {
                lowerBound = Min;
                if(index > 0)
                {
                    upperBound = index-1;
                }
            }

        }


        System.out.println("total" + memory.checkTimes);
        System.out.println("miss" + memory.unhitTimes);

        System.out.println("total" + memory.checkTimes);
        System.out.println("miss" + memory.unhitTimes);

        float hitRate = (float)memory.unhitTimes/(float)memory.checkTimes;
        int hitTimes = memory.checkTimes-memory.unhitTimes;
        MainFrame.LRUResultLabel.setText("<html><p>总次数："+memory.checkTimes+"</p><p>命中："+hitTimes+
                "</p><p>未命中："+memory.unhitTimes+"</p><p>命中率："+hitRate+"</p></html>");
    }




}
