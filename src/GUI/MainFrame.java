package GUI;
import execution.CPU;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
/**
 * Created by danielqiu on 5/12/14.
 */


public class MainFrame extends JFrame implements ActionListener {


    public static JTextArea FIFOTextArea = new JTextArea(20,25);
    public static JTextArea LRUTextArea = new JTextArea(20,25);
    public static JLabel FIFOResultLabel = new JLabel("<html><p>总次数：</p><p>命中：</p><p>未命中：</p><p>命中率：</p></html>");

    public static JLabel LRUResultLabel = new JLabel("<html><p>总次数：</p><p>命中：</p><p>未命中：</p><p>命中率：</p></html>");

    private Button FIFOButton = new Button("开始模拟");
    private Button LRUButton = new Button("开始模拟");


    public MainFrame()
    {
        setSize(800,500);

        setTitle("请求调⻚页存储管理⽅方式模拟");
        Container contentPane = this.getContentPane();

        JLabel FIFOLabel = new JLabel("FIFO算法");
        FIFOLabel.setBounds(160,20,150,50);
        JLabel LRULabel = new JLabel("LRU算法");
        LRULabel.setBounds(570,20,150,50);

        JScrollPane FIFOScrollPane = new JScrollPane(FIFOTextArea);
        FIFOScrollPane.setBounds(20,60,350,300);

        JScrollPane LRUScrollPane = new JScrollPane(LRUTextArea);
        LRUScrollPane.setBounds(420, 60, 350, 300);




        FIFOButton.setBounds(100,400,100,50);
        LRUButton.setBounds(540,400,100,50);
        FIFOResultLabel.setBounds(250,350,150,150);
        LRUResultLabel.setBounds(680,350,150,150);

        contentPane.setLayout(null);
        contentPane.add(FIFOLabel);
        contentPane.add(LRULabel);
        contentPane.add(LRUScrollPane);
        contentPane.add(FIFOScrollPane);
        contentPane.add(FIFOButton);
        contentPane.add(LRUButton);
        contentPane.add(FIFOResultLabel);
        contentPane.add(LRUResultLabel);

        FIFOButton.addActionListener(this);
        LRUButton.addActionListener(this);
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==this.FIFOButton){
            clickFIFOButton();
        }
        else
        {
            clickLRUButton();
        }
    }


    public void clickFIFOButton()
    {

        MainFrame.FIFOTextArea.setText("");
        CPU cpu = new CPU();
        cpu.executeFIFO();
    }

    public void clickLRUButton()
    {
        MainFrame.LRUTextArea.setText("");
        CPU cpu = new CPU();
        cpu.executeLRU();
    }

}
