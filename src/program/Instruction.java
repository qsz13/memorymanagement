package program;

/**
 * Created by danielqiu on 5/11/14.
 */
public class Instruction {

    private int instructionID;
    private int pageNumber;
    private int pageOffset;
    private Page page;

    public Instruction(int instructionID) {
        this.instructionID = instructionID;
        this.pageNumber = instructionID / 4;
        this.pageOffset = instructionID % 4;

    }

    public int getInstructionID() {
        return instructionID;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public int getPageOffset() {
        return pageOffset;
    }

    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }
}
