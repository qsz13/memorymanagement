package program;

import memory.Frame;
import program.Instruction;

import java.util.ArrayList;

/**
 * Created by danielqiu on 5/11/14.
 */
public class Page {

    private int pageID;
    private Frame frame;
    public ArrayList<Instruction> instructionList = new ArrayList<Instruction>(10);



    public Page(int id)
    {
        this.pageID = id;

    }

    public void addInstruction(Instruction ins) {

        this.instructionList.add(ins);
    }

    public Frame getFrame() {
        return frame;
    }

    public void setFrame(Frame frame) {
        this.frame = frame;
    }

    public int getPageID() {
        return pageID;
    }
}
