package program;

import java.util.ArrayList;

/**
 * Created by danielqiu on 5/11/14.
 */
public class Process {

    public ArrayList<Instruction> instructions = new ArrayList<Instruction>(320);
    public ArrayList<Page> pages = new ArrayList<Page>(100);
    private ArrayList<Boolean> executedFlagArray = new ArrayList<Boolean>(320);
    public Process(){

        for (int i = 0; i < 320; i++)
        {
            this.executedFlagArray.add(false);
            Instruction ins = new Instruction(i);
            instructions.add(ins);
            if(i%10 == 0)
            {
                Page page = new Page(i/10);
                pages.add(page);

                page.addInstruction(ins);
                ins.setPage(page);
            }
            else
            {
                Page page = pages.get(i / 10);
                page.addInstruction(ins);
                ins.setPage(page);
            }

        }

    }


    public Boolean allExecuted()
    {
        for(Boolean flag : executedFlagArray)
        {
            if(!flag)
            {
                return false;
            }


        }
        return true;
    }

    public void markExecuted(int index)
    {
        this.executedFlagArray.set(index, true);
    }


}
