package memory;

import program.Instruction;
import program.Page;

/**
 * Created by danielqiu on 5/11/14.
 */
public class Frame {

    private int frameID;

    private Page page;

    private int age = 0;

    private boolean empty = true;

    public Frame(int id)
    {
        this.frameID = id;
    }

    public void loadPage(Page page)
    {
        this.empty = false;
        this.page = page;
    }

    public Boolean checkInstructionInFrame(Instruction instruction) {

        for (Instruction ins : this.page.instructionList)
        {
            if (instruction.getInstructionID() == ins.getInstructionID())
            {
                return true;
            }
        }
        return false;
    }

    public boolean isEmpty()
    {
        return empty;
    }

    public void getOlder()
    {
        if (!isEmpty())
        {
            this.age++;
        }
    }

    public void clearAge()
    {
        this.age = 0;
    }

    public int getAge()
    {
        return this.age;
    }

    public int getFrameID() {
        return frameID;
    }

    public Page getPage() {
        return page;
    }
}

