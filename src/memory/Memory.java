package memory;


import GUI.MainFrame;
import program.Instruction;
import program.Page;

import javax.swing.*;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Created by danielqiu on 5/11/14.
 */
public class Memory {

    public ArrayList<Frame> frameList = new ArrayList<Frame>(4);
    public LinkedList<Frame> frameQueue = new LinkedList<Frame>();
    public int checkTimes = 0;
    public int unhitTimes = 0;
    public boolean [] bitmap = new boolean[4];
    private JTextArea FIFOTextArea = MainFrame.FIFOTextArea;
    private JTextArea LRUTextArea = MainFrame.LRUTextArea;


    public Memory()
    {
        for(int i = 0; i < 4; i ++)
        {
            Frame frame = new Frame(i);
            frameList.add(frame);
            this.bitmap[i] = false;
        }

    }


    public void loadInsructionFIFO(Instruction ins)
    {
        if(!checkInstructionInMemory(ins)) {
            System.out.println("第"+ins.getInstructionID()+"条指令不在内存中，正在请求调页\n");
            FIFOTextArea.append("第" + ins.getInstructionID() + "条指令不在内存中，正在请求调页\n");
            Page page = ins.getPage();
            this.loadPageFIFO(page);

        }
        else
        {

            int index = this.getIndexOfFrame(ins);

            System.out.println("命中第"+index+"块内存区域"+"指令地址为"+ins.getInstructionID()+"\n");
            FIFOTextArea.append("命中第" + index + "块内存区域" + "指令地址为" + ins.getInstructionID() + "\n");


        }
        this.hitFrame(ins.getPage().getFrame());


    }


    public void loadInsructionLRU(Instruction ins)
    {
        if(!checkInstructionInMemory(ins)) {
            System.out.println("第"+ins.getInstructionID()+"条指令不在内存中，正在请求调页\n");
            LRUTextArea.append("第" + ins.getInstructionID() + "条指令不在内存中，正在请求调页\n");

            Page page = ins.getPage();
            this.loadPageLRU(page);

        }
        else
        {

            int index = this.getIndexOfFrame(ins);

            System.out.println("命中第"+index+"块内存区域"+"指令地址为"+ins.getInstructionID()+"\n");
            LRUTextArea.append("命中第" + index + "块内存区域" + "指令地址为" + ins.getInstructionID() + "\n");


        }
        this.hitFrame(ins.getPage().getFrame());


    }

    public Boolean checkInstructionInMemory(Instruction ins) {
        this.checkTimes++;
        for (Frame frame : frameList ) {
            if(frame.isEmpty())
            {
                continue;
            }
            if (frame.checkInstructionInFrame(ins)) {
                return true;
            }
        }
        this.unhitTimes++;
        return false;
    }


    public int getIndexOfFrame(Instruction ins)
    {
        return ins.getPage().getFrame().getFrameID();
    }


    private void hitFrame(Frame frame)
    {

        for (Frame f : this.frameList)
        {
            if (f.equals(frame))
            {
                frame.clearAge();
                continue;
            }
            else
            {
                frame.getOlder();
            }

        }


    }

    private void loadPageFIFO(Page page)
    {
        for(int i = 0; i < 4 ; i++)
        {
            Frame frame = frameList.get(i);
            if(frame.isEmpty()&&this.bitmap[i] == false)
            {
                this.bitmap[i] = true;
                frame.loadPage(page);
                page.setFrame(frame);
                frameQueue.add(frame);
                System.out.println("已将第" + page.getPageID() + "页调入第" + frame.getFrameID() + "块空白区域\n");
                FIFOTextArea.append("已将第" + page.getPageID() + "页调入第" + frame.getFrameID() + "块空白区域\n");
              return;
            }
        }
        Frame firstInFrame = this.frameQueue.getFirst();
        this.frameQueue.removeFirst();

        firstInFrame.getPage().setFrame(null);
        System.out.println("已将第" + firstInFrame.getPage().getPageID() + "页从第" + firstInFrame.getFrameID() + "块区域调出\n");
        FIFOTextArea.append("已将第"+firstInFrame.getPage().getPageID()+"页从第"+firstInFrame.getFrameID()+"块区域调出\n");

        firstInFrame.loadPage(page);
        page.setFrame(firstInFrame);
        System.out.println("已将第" + page.getPageID() + "页调入第" + firstInFrame.getFrameID() + "块区域\n");
        FIFOTextArea.append("已将第" + page.getPageID() + "页调入第" + firstInFrame.getFrameID() + "块区域\n");

        this.frameQueue.add(firstInFrame);
    }

    private void loadPageLRU(Page page)
    {
        for(int i = 0; i < 4 ; i++)
        {
            Frame frame = frameList.get(i);
            if(frame.isEmpty()&&this.bitmap[i] == false)
            {
                this.bitmap[i] = true;
                frame.loadPage(page);
                page.setFrame(frame);
                frameQueue.add(frame);
                System.out.println("已将第" + page.getPageID() + "页调入第" + frame.getFrameID() + "块空白区域\n");
                LRUTextArea.append("已将第" + page.getPageID() + "页调入第" + frame.getFrameID() + "块空白区域\n");
                return;
            }
        }

        Frame oldestFrame = this.getOldestFrame();
        this.frameQueue.removeFirst();
        oldestFrame.getPage().setFrame(null);
        System.out.println("已将第" + oldestFrame.getPage().getPageID() + "页从第" + oldestFrame.getFrameID() + "块区域调出\n");
        LRUTextArea.append("已将第"+oldestFrame.getPage().getPageID()+"页从第"+oldestFrame.getFrameID()+"块区域调出\n");

        oldestFrame.loadPage(page);
        page.setFrame(oldestFrame);
        System.out.println("已将第" + page.getPageID() + "页调入第" + oldestFrame.getFrameID() + "块区域\n");
        LRUTextArea.append("已将第" + page.getPageID() + "页调入第" + oldestFrame.getFrameID() + "块区域\n");


        this.frameQueue.add(oldestFrame);




    }


    private Frame getOldestFrame()
    {
        int maxAge = 0;
        int maxIndex = 0;
        for (int i = 0; i < 4; i++)
        {
            Frame f = this.frameList.get(i);
            if (f.getAge() > maxAge)
            {
                maxAge = f.getAge();
                maxIndex = i;

            }
        }

        return frameList.get(maxIndex);
    }
    



}


